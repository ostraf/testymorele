package moreleNet;
import io.cucumber.java.After;
import moreleNetPages.MainPage;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.concurrent.TimeUnit;

public class MoreleNetStepDefinitions {
    WebDriver driver;
    WebDriverWait wait;
    MainPage mainPage;
    @Before
    public void setup(){
        System.setProperty("webdriver.chrome.driver","chromedriver/chromedriver.exe");
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Given("I am on the moreleNet main page")
    public void i_am_on_the_moreleNet_main_page(){
        mainPage = new MainPage(driver);
    }

    @Then("I should see the main logo")
    public void i_should_see_the_main_logo() {
        wait.until(ExpectedConditions.visibilityOf(mainPage.mainLogo));
        Assert.assertTrue(mainPage.mainLogo.isDisplayed());
    }
    @Then("I should see the search bar")
    public void i_should_see_the_search_bar() {
        wait.until(ExpectedConditions.visibilityOf(mainPage.searchBar));
        Assert.assertTrue(mainPage.mainLogo.isDisplayed());
    }
    @Then("I should see the Free delivery button")
    public void i_should_see_the_free_delivery_button() {
        wait.until(ExpectedConditions.visibilityOf(mainPage.freeDeliveryBtn));
        Assert.assertTrue(mainPage.freeDeliveryBtn.isDisplayed());
    }
    @Then("I should see the Contact button")
    public void i_should_see_the_contact_button() {
        wait.until(ExpectedConditions.visibilityOf(mainPage.contactBtn));
        Assert.assertTrue(mainPage.contactBtn.isDisplayed());
    }
    @Then("I should see the Shopping Lists button")
    public void i_should_see_the_shopping_lists_button() {
        wait.until(ExpectedConditions.visibilityOf(mainPage.shoppingListsBtn));
        Assert.assertTrue(mainPage.shoppingListsBtn.isDisplayed());
    }
    @Then("I should see the Login or Create account button")
    public void i_should_see_the_login_or_create_account_button() {
        wait.until(ExpectedConditions.visibilityOf(mainPage.loginBtn));
        Assert.assertTrue(mainPage.loginBtn.isDisplayed());
    }
    @Then("I should see the Shopping Cart button")
    public void i_should_see_the_shopping_cart_button() {
        wait.until(ExpectedConditions.visibilityOf(mainPage.shoppingCartBtn));
        Assert.assertTrue(mainPage.shoppingCartBtn.isDisplayed());
    }
    @Then("I should see the Laptops button")
    public void i_should_see_the_laptops_button() {
        wait.until(ExpectedConditions.visibilityOf(mainPage.laptopsBtn));
        Assert.assertTrue(mainPage.laptopsBtn.isDisplayed());
    }
    @Then("I should see the Computers button")
    public void i_should_see_the_computers_button() {
        wait.until(ExpectedConditions.visibilityOf(mainPage.computersBtn));
        Assert.assertTrue(mainPage.computersBtn.isDisplayed());
    }
    @Then("I should see the Computer Hardware button")
    public void i_should_see_the_computer_hardware_button() {
        wait.until(ExpectedConditions.visibilityOf(mainPage.computerHardwareBtn));
        Assert.assertTrue(mainPage.computerHardwareBtn.isDisplayed());
    }
    @Then("I should see the Gaming button")
    public void i_should_see_the_gaming_button() {
        wait.until(ExpectedConditions.visibilityOf(mainPage.gamingBtn));
        Assert.assertTrue(mainPage.gamingBtn.isDisplayed());
    }
    @Then("I should see the Smartphones and Smartwatches button")
    public void i_should_see_the_smartphones_and_smartwatches_button() {
        wait.until(ExpectedConditions.visibilityOf(mainPage.smartphonesAndSmartwatchesBtn));
        Assert.assertTrue(mainPage.smartphonesAndSmartwatchesBtn.isDisplayed());
    }
    @Then("I should see the TV's and Audio button")
    public void i_should_see_the_tvs_and_audio_button() {
        wait.until(ExpectedConditions.visibilityOf(mainPage.tvsAndAudioBtn));
        Assert.assertTrue(mainPage.tvsAndAudioBtn.isDisplayed());
    }
    @Then("I should see the Big AGD button")
    public void i_should_see_the_big_agd_button() {
        wait.until(ExpectedConditions.visibilityOf(mainPage.bigAgdBtn));
        Assert.assertTrue(mainPage.bigAgdBtn.isDisplayed());
    }
    @Then("I should see the Small AGD button")
    public void i_should_see_the_small_agd_button() {
        wait.until(ExpectedConditions.visibilityOf(mainPage.smallAgdBtn));
        Assert.assertTrue(mainPage.smallAgdBtn.isDisplayed());
    }
    @Then("I should see the Home and Garden button")
    public void i_should_see_the_home_and_garden_button() {
        wait.until(ExpectedConditions.visibilityOf(mainPage.homeAndGardenBtn));
        Assert.assertTrue(mainPage.homeAndGardenBtn.isDisplayed());
    }
    @Then("I should see the Sports and Recreation button")
    public void i_should_see_the_sports_and_recreation_button() {
        wait.until(ExpectedConditions.visibilityOf(mainPage.sportsAndRecreationBtn));
        Assert.assertTrue(mainPage.sportsAndRecreationBtn.isDisplayed());
    }






    @After
    public void cleanup()
    {
        driver.quit();
    }
}
