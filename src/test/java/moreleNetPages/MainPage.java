package moreleNetPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage {
    WebDriver driver;
    String url = "https://morele.net";

    @FindBy(xpath = "//img[@alt=\"morele\"]")
    public WebElement mainLogo;
    @FindBy(xpath = "//div[@class=\"h-quick-search\"]//input[@name=\"search\"]")
    public WebElement searchBar;
    @FindBy(xpath = "//a[@href=\"/darmowa-dostawa\"][@class=\"h-control-btn\"]")
    public WebElement freeDeliveryBtn;
    @FindBy(xpath = "//div[@class=\"h-control h-contact-control h-control-has-popup\"]")
    public WebElement contactBtn;
    @FindBy(xpath = "//div[@class=\"h-control h-shopping-lists-control\"]")
    public WebElement shoppingListsBtn;
    @FindBy(xpath = "//div[@class=\"h-control h-user-control\"]")
    public WebElement loginBtn;
    @FindBy(xpath = "//div[@class=\"h-control h-basket-control\"]")
    public WebElement shoppingCartBtn;


    @FindBy(xpath = "//a[@href=\"/laptopy/\"]/span")
    public WebElement laptopsBtn;
    @FindBy(xpath = "//a[@href=\"/komputery/\"]/span")
    public WebElement computersBtn;
    @FindBy(xpath = "//a[@href=\"/podzespoly-komputerowe/\"]/span")
    public WebElement computerHardwareBtn;
    @FindBy(xpath = "//a[@href=\"/gaming/\"]/span")
    public WebElement gamingBtn;
    @FindBy(xpath = "//a[@href=\"/smartfony-i-smartwatche/\"]/span")
    public WebElement smartphonesAndSmartwatchesBtn;
    @FindBy(xpath = "//a[@href=\"/telewizory-i-audio/\"]/span")
    public WebElement tvsAndAudioBtn;
    @FindBy(xpath = "//a[@href=\"/agd/\"]/span")
    public WebElement bigAgdBtn;
    @FindBy(xpath = "//a[@href=\"/male-agd/\"]/span")
    public WebElement smallAgdBtn;
    @FindBy(xpath = "//a[@href=\"/dom-i-ogrod/\"]/span")
    public WebElement homeAndGardenBtn;
    @FindBy(xpath = "//a[@href=\"/sport-i-rekreacja/\"]/span")
    public WebElement sportsAndRecreationBtn;



    public MainPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
        driver.get(url);
    }

}
