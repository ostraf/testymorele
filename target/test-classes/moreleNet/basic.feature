Feature: Visibility of main page elements
  Scenario: Visibility of main page logo
    Given I am on the moreleNet main page
    Then I should see the main logo

  Scenario: Visibility of search bar
    Given I am on the moreleNet main page
    Then I should see the search bar

  Scenario: Visibility of search bar
    Given I am on the moreleNet main page
    Then I should see the Free delivery button

  Scenario: Visibility of search bar
    Given I am on the moreleNet main page
    Then I should see the Contact button

  Scenario: Visibility of search bar
    Given I am on the moreleNet main page
    Then I should see the Shopping Lists button

  Scenario: Visibility of search bar
    Given I am on the moreleNet main page
    Then I should see the Login or Create account button

  Scenario: Visibility of search bar
    Given I am on the moreleNet main page
    Then I should see the Shopping Cart button

  Scenario: Visibility of search bar
    Given I am on the moreleNet main page
    Then I should see the Laptops button

  Scenario: Visibility of search bar
    Given I am on the moreleNet main page
    Then I should see the Computers button

  Scenario: Visibility of search bar
    Given I am on the moreleNet main page
    Then I should see the Computer Hardware button

  Scenario: Visibility of search bar
    Given I am on the moreleNet main page
    Then I should see the Gaming button

  Scenario: Visibility of search bar
    Given I am on the moreleNet main page
    Then I should see the Smartphones and Smartwatches button

  Scenario: Visibility of search bar
    Given I am on the moreleNet main page
    Then I should see the TV's and Audio button

  Scenario: Visibility of search bar
    Given I am on the moreleNet main page
    Then I should see the Big AGD button

  Scenario: Visibility of search bar
    Given I am on the moreleNet main page
    Then I should see the Small AGD button

  Scenario: Visibility of search bar
    Given I am on the moreleNet main page
    Then I should see the Home and Garden button

  Scenario: Visibility of search bar
    Given I am on the moreleNet main page
    Then I should see the Sports and Recreation button